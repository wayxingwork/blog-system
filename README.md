# BlogSystem

#### 介绍
第10章代码-简化版1

#### 安装教程

1.  Mysql导入 doc/blog_system.sql 

3.  运行 主程序

#### 使用说明

1.  http://localhost:8080/

![输入图片说明](doc/img/1.PNG)

2. 登录后台 

用户名：admin  密码 123456

普通用户： 李四 密码 123456

![输入图片说明](doc/img/2.PNG)

3. 后台界面

![输入图片说明](doc/img/3.PNG)